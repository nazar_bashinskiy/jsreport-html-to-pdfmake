const htmlPdf = require('./html-to-pdf')

module.exports = reporter => {
    reporter.documentStore.model.entityTypes['TemplateType'].contentType = { type: 'Edm.String' }
    reporter.documentStore.model.entityTypes['TemplateType'].fileExtension = { type: 'Edm.String' }
    reporter.documentStore.model.entityTypes['TemplateType'].contentDisposition = { type: 'Edm.String' }

    reporter.extensionsManager.recipes.push({
        name: 'html-to-pdfmake',
        execute: (request, response) => {
            response.meta.contentType = request.template.contentType || 'application/pdf'
            response.meta.fileExtension = request.template.fileExtension || '.pdf'
            response.meta.contentDisposition = `attachment; filename="report.pdf"`
            htmlPdf(response.content.toString(), buf => response.content = buf)
        }
    })
}