# jsreport-html-to-pdfmake
It is custom receipe for [jsreport-core](https://github.com/jsreport/jsreport-core "jsreport-core") to genereate docx from html template.
Based on [html-to-pdfmake](https://github.com/Aymkdn/html-to-pdfmake)
## Usage 


    const jsreport = require('jsreport-core')();
    const handlebars = require('jsreport-handlebars');
    const htmlPdfmake = require('jsreport-html-to-pdfmake');
    
    const fs = require('fs')
    
    async function render(text){
        await jsreport.init();
        jsreport.use(handlebars);
        jsreport.use(htmlPdfmake)
    
        const pdf = await jsreport.render({
            template: {
                content: text,
                engine: 'handlebars',
                recipe: 'html-to-pdfmake',
            },
            data: {
                test: 'test2'
            }
        });
    
        return pdf
    }
    
    render('<div><h1><span data-type="highlight" style="background-color: #ffff00;"><strong>{{test}}</strong></span></h1></div>')
        .then(res => fs.writeFileSync('./temp.pdf', res.content))