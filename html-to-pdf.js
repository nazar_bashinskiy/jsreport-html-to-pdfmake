const PdfPrinter = require('pdfmake');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const { window } = new JSDOM('')
const htmlToPdfMake = require('html-to-pdfmake');
const fs = require('fs')

module.exports = createPdf

function createPdf(htmlstr, callback){
    const fonts = {
        Roboto: {
            normal: __dirname + '/fonts/regular.ttf',
            bold: __dirname + '/fonts/medium.ttf',
            italics:  __dirname + '/fonts/italic.ttf',
            bolditalics:  __dirname + '/fonts/mediumitalic.ttf'
        }
    };

    const printer = new PdfPrinter(fonts);
    const html = htmlToPdfMake(htmlstr, {window:window, tableAutoSize:true});

    const docDefinition = {
        content: [
            html
        ],
    };

    const pdfDoc = printer.createPdfKitDocument(docDefinition, {bufferPages: true});

    let chunks = [];
    let result;

    pdfDoc.on('data', function (chunk) {
        chunks.push(chunk);
    });
    pdfDoc.on('end', function () {
        result = Buffer.concat(chunks);
        callback(result);
    });
    pdfDoc.end();
}
