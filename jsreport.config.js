module.exports = {
    name: 'html-to-pdfmake',
    dependencies: ['templates'],
    main: 'lib.js',
    embeddedSupport: true
}