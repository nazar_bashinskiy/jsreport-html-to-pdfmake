import { Extension } from 'jsreport-core';

declare function JsReportHtmlToPdfmake(): Extension;

export = JsReportHtmlToPdfmake;